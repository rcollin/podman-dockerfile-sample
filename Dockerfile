FROM debian:11
RUN apt -y update && apt -y install apache2 git  && apt clean all
COPY index.html /var/www/html/index.html
EXPOSE 80
#ENTRYPOINT /usr/sbin/httpd -DFOREGROUND
ENTRYPOINT apachectl -D FOREGROUND
