# podman-dockerfile-sample

build a sample docker image from Dockerfile based on Debian

## build :

```sudo podman build --tag debian:myhttpd -f ./Dockerfile```

## run

```sudo podman run -d -p 8080:80 --name myhttpd --rm debian:myhttpd```

## check

```curl http://localhost:8080```
